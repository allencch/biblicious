# Guide

For first time to download Studium Biblicum from `sbofmhk`, needs to edit `sbofmhk.html`, to empty the `.links` part, and enable the `displayLinks()` function.

Then run with `php -S 127.0.0.1:3000`

This will generate all the links to the chapters.

Using web browser, copy the HTML content, and fill in the `.links` element.

Then disable the `displayLinks()`.

Then can use `httrack` to download the pages.
