const utils = require('../utils');
const dbService = require('../dbService');
const Chapter = require('../Chapter');
const Verse = require('../Verse');

const allBooks = require('../constants/books.json');
const BIBLE = 'Studium Biblicum';

const convertCellsToVerse = cells => {
  if (!cells.join('').trim()) {
    return null;
  }

  const match = cells[0].match(/(\d+):(\d+)/);
  if (match && match[2]) {
    return new Verse(Number(match[2]), cells[1]);
  }
  return null;
};

const convertRowToVerse = row => {
  const cells = [];
  Array.from(row.cells).forEach((cell, index) => {
    if (index === 2) { // Skip the 3rd column
      return;
    }
    const text = utils.brToNewline(cell);
    cells.push(text);
  });
  return convertCellsToVerse(cells);
};

const convertTableToVerses = table => {
  const verses = [];
  Array.from(table.rows).forEach(row => {
    const verse = convertRowToVerse(row);
    if (verse) {
      verses.push(verse);
    }
  });
  return verses;
};

const extractChapter = async (book, folder, chapterNum) => {
  const doc = await utils.getDocumentFromPage(`http://localhost:3000/dsbiblecentre/sbbible.dsbiblecentre.org/part_1/${folder}/${chapterNum}.html`);

  const table = doc.querySelector('tbody');
  const verses = convertTableToVerses(table);
  const chapter = new Chapter();
  verses.forEach(v => {
    chapter.addVerse(v);
  });

  return dbService.saveChapterToDB({
    bible: BIBLE,
    book,
    chapterNum,
    chapter,
    successFn: (verse) => {
      console.log(`Book ${book} chapter ${chapterNum} verse ${verse.number} added`);
    }
  });
};

(async () => {
  const booksOfInterest = [{
    name: 'Genesis',
    totalChapter: 50,
    folder: 3
  }, {
    name: 'Exodus',
    totalChapter: 40,
    folder: 4
  }];

  for (let i = 0; i < allBooks.length; i++) {
    const book = allBooks[i];
    if (booksOfInterest.map(b => b.name).indexOf(book.name) < 0) {
      continue;
    }
    const bookOfInterest = booksOfInterest.find(b => b.name === book.name);
    const chapterNums = Array.from(new Array(bookOfInterest.totalChapter)).map((dummy, index) => index + 1);
    for (let j = 0; j < chapterNums.length; j++) {
      const chapterNum = chapterNums[j];
      await extractChapter(book.name, bookOfInterest.folder, chapterNum);
    }
  }
})();
