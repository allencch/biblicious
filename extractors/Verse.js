class Verse {
  constructor(number, text, extraChapter = null) {
    this.number = number;
    this.text = text;
    this.extraChapter = extraChapter;
  }

  getNumber() {
    return this.number;
  }
  getText() {
    return this.text;
  }

  setExtraChapter(chapter) {
    this.extraChapter = chapter;
  }

  getExtraChapter() {
    return this.extraChapter;
  }
}

module.exports = Verse;
