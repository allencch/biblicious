// Create URL mapper for the sbofmhk
// Because the website uses PHP, HTTrack downloads the PHP files
// HTML, there for the naming is not following the correct sequence.
// Need to use the created index file (sbofmhk.html) to get the
// URL mapping.

const fs = require('fs');
const utils = require('../utils');

const getUrlMap = async () => {
  const doc = await utils.getDocumentFromPage('http://localhost:3100/sbofmhk/127.0.0.1_3000/sbofmhk.html');
  const links = Array.from(doc.querySelectorAll('.links a'));
  return links.map(link => ({
    name: link.textContent,
    url: link.href
  }));
};

const main = async () => {
  const map = await getUrlMap();
  fs.writeFileSync('urlMap.json', JSON.stringify(map));
  console.log('Convert to URL map done');
};

main();
