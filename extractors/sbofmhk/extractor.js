// Extractor for sbofmhk
const utils = require('../utils');
const dbService = require('../dbService');
const Chapter = require('../Chapter');
const Verse = require('../Verse');

const chapterMap = require('./urlMap.json');
const Books = require('../constants/books.json');
const SectionExtractor = require('./sectionExtractor');
const SpeakerExtractor = require('./speakerExtractor');

const BIBLE = 'Studium Biblicum';

// Options to decide what to do/don't
// NOTE: Do not run the script will all options.
// You should run with extractChapters first, followed by extractSections
const OPTIONS = {
  extractChapters: false,
  extractSections: false,
  extractSongSpeakers: true
};

const getExtraChapterFromRow = row => {
  let extraChapter = null;
  Array.from(row.cells).forEach((cell, index) => {
    if (index !== 0) return;

    const text = utils.brToNewline(cell);
    const m = text.match(/\[補錄(.)\]/);
    if (m) {
      extraChapter = m[1];
    }
  });
  return extraChapter;
};

const convertCellsToVerse = (cells, extraChapter = null) => {
  if (!cells.join('').trim()) {
    return null;
  }

  // Esther has extra chapter from Greek text
  if (extraChapter) {
    const match = cells[0].match(/^(\d+)$/);
    if (match && match[1]) {
      return new Verse(Number(match[1]), cells[1], extraChapter);
    }
  }

  // Usual books
  if (/(\d+):(\d+)/.test(cells[0])) {
    const match = cells[0].match(/(\d+):(\d+)/);
    if (match && match[2]) {
      return new Verse(Number(match[2]), cells[1]);
    }
  } else { // Jude
    const match = cells[0].match(/^(\d+)/);
    if (match && match[1]) {
      return new Verse(Number(match[1]), cells[1]);
    }
  }
  return null;
};

const convertRowToVerse = (row, extraChapter = null) => {
  const cells = [];
  Array.from(row.cells).forEach((cell, index) => {
    if (index === 2) { // Skip the 3rd column
      return;
    }
    const text = utils.brToNewline(cell).replace(/\*/g, '');
    cells.push(text);
  });
  return convertCellsToVerse(cells, extraChapter);
};

const convertTableToVerses = table => {
  const verses = [];
  let extraChapter;
  Array.from(table.rows).forEach(row => {
    const result = getExtraChapterFromRow(row);
    if (result) extraChapter = result;

    const verse = convertRowToVerse(row, extraChapter);
    if (verse) {
      verses.push(verse);
    }
  });
  return verses;
};

const extractChapter = async (chapterObj, book, chapterNum) => {
  // Use extra "a/" directory, because of the URL mapper has ".."
  const doc = await utils.getDocumentFromPage(`http://localhost:3100/sbofmhk/a/${chapterObj.url}`);

  const table = doc.querySelectorAll('tbody')[2]; // 3rd table

  if (OPTIONS.extractSections) {
    await SectionExtractor.extractSection(table, BIBLE, book, chapterNum);
  } else if (OPTIONS.extractSongSpeakers) {
    await SpeakerExtractor.extractSpeaker(table, BIBLE, book, chapterNum);
  }

  const verses = convertTableToVerses(table, chapterNum);
  const chapter = new Chapter();
  verses.forEach(v => {
    chapter.addVerse(v);
  });

  if (OPTIONS.extractChapters) {
    await dbService.saveChapterToDB({
      bible: BIBLE,
      book,
      chapterNum,
      chapter,
      successFn: (verse) => {
        console.log(`Book ${book} chapter ${chapterNum} verse ${verse.number} added`);
      }
    });
  }
};

const getChapterUrl = name => {
  return chapterMap.find(b => b.name === name);
};

const getBookByZhName = name => {
  return Books.find(b => b.zhName === name);
};

const main = async () => {
  const booksOfInterest = [
    {
      zhName: '創世紀',
    },
    {
      zhName: '出谷紀',
    },
    {
      zhName: '肋未紀',
    },
    {
      zhName: '戶籍紀',
    },
    {
      zhName: '申命紀',
    },
    {
      zhName: '若蘇厄書',
    },
    {
      zhName: '民長紀',
    },
    {
      zhName: '盧德傳',
    },
    {
      zhName: '撒慕爾紀上',
    },
    {
      zhName: '撒慕爾紀下',
    },
    {
      zhName: '列王紀上',
    },
    {
      zhName: '列王紀下',
    },
    {
      zhName: '編年紀上',
    },
    {
      zhName: '編年紀下',
    },
    {
      zhName: '厄斯德拉上',
    },
    {
      zhName: '厄斯德拉下',
    },
    {
      zhName: '多俾亞傳',
    },
    {
      zhName: '友弟德傳',
    },
    {
      zhName: '艾斯德爾傳',
    },
    {
      zhName: '瑪加伯上',
    },
    {
      zhName: '瑪加伯下',
    },
    {
      zhName: '約伯傳',
    },
    {
      zhName: '聖詠集',
    },
    {
      zhName: '箴言',
    },
    {
      zhName: '訓道篇',
    },
    {
      zhName: '雅歌',
    },
    {
      zhName: '智慧篇',
    },
    {
      zhName: '德訓篇',
    },
    {
      zhName: '依撒意亞',
    },
    {
      zhName: '耶肋米亞',
    },
    {
      zhName: '哀歌',
    },
    {
      zhName: '巴路克',
    },
    {
      zhName: '厄則克耳',
    },
    {
      zhName: '達尼爾',
    },
    {
      zhName: '歐瑟亞',
    },
    {
      zhName: '岳厄爾',
    },
    {
      zhName: '亞毛斯',
    },
    {
      zhName: '亞北底亞',
    },
    {
      zhName: '約納',
    },
    {
      zhName: '米該亞',
    },
    {
      zhName: '納鴻',
    },
    {
      zhName: '哈巴谷',
    },
    {
      zhName: '索福尼亞',
    },
    {
      zhName: '哈蓋',
    },
    {
      zhName: '匝加利亞',
    },
    {
      zhName: '瑪拉基亞',
    },
    {
      zhName: '瑪竇福音',
    },
    {
      zhName: '馬爾谷福音',
    },
    {
      zhName: '路加福音',
    },
    {
      zhName: '若望福音',
    },
    {
      zhName: '宗徒大事錄',
    },
    {
      zhName: '羅馬書',
    },
    {
      zhName: '格林多前書',
    },
    {
      zhName: '格林多後書',
    },
    {
      zhName: '迦拉達書',
    },
    {
      zhName: '厄弗所書',
    },
    {
      zhName: '斐理伯書',
    },
    {
      zhName: '哥羅森書',
    },
    {
      zhName: '得撒洛尼前書',
    },
    {
      zhName: '得撒洛尼後書',
    },
    {
      zhName: '弟茂德前書',
    },
    {
      zhName: '弟茂德後書',
    },
    {
      zhName: '弟鐸書',
    },
    {
      zhName: '費肋孟書',
    },
    {
      zhName: '希伯來書',
    },
    {
      zhName: '雅各伯書',
    },
    {
      zhName: '伯多祿前書',
    },
    {
      zhName: '伯多祿後書',
    },
    {
      zhName: '若望一書',
    },
    {
      zhName: '若望二書',
    },
    {
      zhName: '若望三書',
    },
    {
      zhName: '猶達書',
    },
    {
      zhName: '若望默示錄',
    },
  ];

  for (let i = 0; i < (booksOfInterest.length); i++) {
    const { zhName, name: enName, totalChapter } = getBookByZhName(booksOfInterest[i].zhName);

    const chapNums = Array.from(
      new Array(totalChapter)
    ).map((dummy, index) => index + 1);

    for (let j = 0; j < chapNums.length; j++) {
      const chapNum = chapNums[j];
      const chapter = getChapterUrl(`${zhName} ${chapNum}`);
      await extractChapter(chapter, enName, chapNum);
    }
  }
  console.log('Done');
};


if (!module.parent) {
  main();
}

module.exports = {
  getExtraChapterFromRow,
  convertRowToVerse
};
