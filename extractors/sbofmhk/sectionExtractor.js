const DbService = require('dbService');

// Section is the big title that tells which
// chapters are until it
const isSection = text => {
  return !isVerse(text)
    && text.length < 20 // Special case to skip Songs
    && (
      /[\u4E00-\u9FCC]+（[0-9:\-]+）/.test(text)
        || /卷.+/.test(text)
    );
};

const isChapter = text => {
  return /^第.+[章篇]$/.test(text.trim());
};

const getExtraChapter = text => {
  const m = text.match(/\[補錄(.)\]/);
  if (m) {
    return m[1];
  }
  return null;
};

const isExtraChapter = text => {
  const extraChapter = getExtraChapter(text);
  return Boolean(extraChapter);
};

const isVerse = text => {
  return /^(\d+):(\d+)/.test(text)
    || /^(\d+)/.test(text);
};

const getPartialVerse = row => {
  if (!row.cells[0]) return null;

  const boldText = row.cells[0].querySelector('b');
  if (boldText) return null;

  let cellText = row.cells[0].textContent.trim();
  if (/^\d+(:\d+)?$/.test(cellText)) {
    return null;
  } else if (cellText) {
    return cellText; // 1 Timothy chapter 7
  }

  cellText = row.cells[1] && row.cells[1].textContent.trim();
  return cellText || null;
};

const isTitle = (text, bold = false) => {
  return bold &&
    !isSection(text) &&
    !isChapter(text) &&
    !isVerse(text) &&
    !/^\(寓意\)/.test(text); // Skip Songs
};

const addToTitles = (titles, title, options = {}) => {
  titles.push({ title });
};

const getChapterAndVerse = (text, extraChapter = null) => {
  if(/^(\d+):(\d+)/.test(text)) {
    const match = text.match(/^(\d+):(\d+)/);
    if (match && match[2]) {
      return [match[1], match[2]];
    }
  } else {
    const match = text.match(/^(\d+)/);
    if (!extraChapter && match && match[1]) {
      return ['1', match[1]]; // Assuming first chapter, if it is Jude
    } else if (extraChapter && match && match[1]) {
      return [extraChapter, match[1], true];
    }
  }
  return [];
};

const updateLastTitle = (titles, text, extraChapter = null) => {
  if (titles.length === 0) return;

  const lastIndex = titles.length - 1;
  if (titles[lastIndex].verse) return; // Skip if there is verse already

  const [chapter, verse, isExtraChapter] = getChapterAndVerse(text, extraChapter);
  titles[lastIndex].verse = {
    isExtraChapter,
    chapter,
    verse
  };
};

const addPartialVerse = (titles, text, chapter, verse, prepend = false) => {
  if (titles.length === 0) return;

  const lastIndex = titles.length - 1;
  if (titles[lastIndex].verse) return; // Skip if there is verse already

  titles[lastIndex].verse = { chapter, verse, text, prepend };
};

const getText = row => {
  const clone = row.cloneNode(true);
  const ref = clone.querySelector('.r');
  if (ref) {
    ref.remove();
  }
  return clone.textContent.trim().replace(/\s+/, ' ');
};

const getSectionOfTable = table => {
  const titles = [];
  let prevChapter = null;
  let prevVerse = null;
  let extraChapter = null;
  let delayAddPartial = false;
  let prevPartialVerse = '';
  Array.from(table.rows).forEach(row => {
    if (!row) return;

    const text = getText(row);;
    if (isSection(text)) {
      addToTitles(titles, text);
      return;
    }

    const partialVerse = getPartialVerse(row);
    if (partialVerse) {
      if (!prevChapter || !prevVerse) {
        prevPartialVerse = partialVerse;
        delayAddPartial = true;
      } else {
        addPartialVerse(titles, partialVerse, prevChapter, prevVerse);
      }
    }

    if (!row.cells[0]) return;

    const cellText = row.cells[0].textContent.trim();
    const boldText = row.cells[0].querySelector('b');
    if (!cellText.length) return;

    if (isTitle(cellText, Boolean(boldText))) {
      if (isExtraChapter(cellText)) {
        extraChapter = getExtraChapter(text);
      }
      addToTitles(titles, cellText);
      return;
    }

    if (isVerse(cellText)) {
      const [chapter, verse] = getChapterAndVerse(cellText);
      prevChapter = chapter;
      prevVerse = verse;

      if (delayAddPartial) {
        addPartialVerse(titles, prevPartialVerse, prevChapter, prevVerse, true);
        delayAddPartial = false;
        prevPartialVerse = '';
      }

      updateLastTitle(titles, cellText, extraChapter);
    }
  });
  return titles;
};

const extractSection = async (table, bible, book, chapterNum) => {
  const titles = getSectionOfTable(table);
  await DbService.saveTitles(titles, bible, book);
  console.log(`Book ${book} titles chapter ${chapterNum} saved`);
};

module.exports = {
  isSection,
  isTitle,
  getSectionOfTable,
  getChapterAndVerse,
  extractSection
};
