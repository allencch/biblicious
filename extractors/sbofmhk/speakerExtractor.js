const { getChapterAndVerse } = require('./sectionExtractor');
const { saveSongSpeakers } = require('../dbService');

const getSpeaker = row => {
  if (row.children.length !== 3) return null;

  const col1 = row.children[0].textContent.trim();
  if (col1) return null;

  const col3 = row.children[2].textContent.trim();
  if (col3) return null;

  const col2 = row.children[1].children[0];
  if (col2.tagName !== 'B') return null;

  return col2.textContent;
};

const getSpeakers = (table) => {
  const speakers = [];

  let speaker = null;
  for (let i = 0; i < table.rows.length; i++) {
    const row = table.rows[i];
    if (!row) continue;

    if (!speaker) {
      speaker = getSpeaker(row);
      if (!speaker) continue;
    }

    const [chapter, verse] = getChapterAndVerse(row.textContent.trim());
    if (!chapter || !verse) continue;

    speakers.push({ speaker, chapter, verse });
    speaker = null;
  }

  return speakers;
};

const extractSpeaker = async (table, bible, book, chapterNum) => {
  if (book !== 'Song of Songs') return;

  const speakers = getSpeakers(table);
  await saveSongSpeakers(speakers, bible, book);
  console.log(`Songs chapter ${chapterNum} saved`);
};

module.exports = {
  getSpeakers,
  extractSpeaker
};
