const { expect } = require('chai');
const { getDocumentFromString } = require('utils');
const SectionExtractor = require('sbofmhk/sectionExtractor');

describe('Section extractor', () => {
  it('extracts section titles for saving', () => {
    const html = `<table>
      <tr valign=top>
        <td width=60><b>前編</b></td>
        <td width=590><b>太古史（1-11）</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=750 colspan=3><br></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2><b>第一章</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=750 colspan=3><br></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2><b>天地萬物的創造</b></td>
        <td width=100 class=r>2:4-25<br>詠 148</td>
      </tr>
      <tr valign=top>
        <td width=60>1:1</td>
        <td width=590>在起初天主創造了天地。</td>
        <td width=100 class=r>約 38-39<br>希 11:3<br>詠 8; 104<br>德 24:5<br>箴 8:22-31<br>若 1:1-3<br>哥 1:15-17</td>
      </tr>
      <tr valign=top>
        <td width=60>1:2</td>
        <td width=590>大地還是混沌空虛，深淵上還是一團黑暗，天主的神在水面上運行。</td>
        <td width=100 class=r></td>
      </tr>
    </table>`;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result.length).to.equal(2);
    expect(result[0].title).to.equal('前編 太古史（1-11）');
    expect(result[1].title).to.equal('天地萬物的創造');
    expect(result[1].verse.chapter).to.equal('1');
    expect(result[1].verse.verse).to.equal('1');
  });

  it('extracts section for broke down verse (Neh)', () => {
    const html = `<table>
      <tr valign=top>
        <td width=60>7:71</td>
        <td width=590>其餘民眾捐獻的，共計二萬金〈達理克〉，二千銀〈米納〉，司祭長衣六十七件。</td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=60>7:72</td>
        <td width=590>司祭、<u>肋未</u>人、守門者、歌詠者、獻身者和全<u>以色列</u>，各住在本城內。</td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=750 colspan=3><br></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2><b>宣讀法律書</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=60></td>
        <td width=590>到了七月，當時<u>以色列</u>子民還各在本城裏。</td>
        <td width=100 class=r></td>
      </tr>
    </table>`;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result[0].verse.chapter).to.equal('7');
    expect(result[0].verse.verse).to.equal('72');
    expect(result[0].verse.text).to.equal('到了七月，當時以色列子民還各在本城裏。');
  });

  it('extracts section for broke down verse (1 Tim)', () => {
    const html = `<table>
      <tr valign=top>
        <td width=60>6:21</td>
        <td width=590>有些人自充有這知識，但終於失落了信德。</td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=750 colspan=3><br></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2><b>祝福辭</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2>願恩寵與你們同在！</td>
        <td width=100 class=r></td>
      </tr>
    </table>`;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result[0].verse.chapter).to.equal('6');
    expect(result[0].verse.verse).to.equal('21');
    expect(result[0].verse.text).to.equal('願恩寵與你們同在！');
  });

  it('extracts broken verse (Psalm 107)', () => {
    const html = `<table>
      <tr valign=top>
        <td width=500 colspan=2><b>第一○七篇（106）</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=750 colspan=3><br></td>
      </tr>
      <tr valign=top>
        <td width=500 colspan=2><b>感謝施救的天主</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=60></td>
        <td width=590>亞肋路亞！</td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=60>107:1</td>
        <td width=590>請你們向上主讚頌，因為他是美善寬仁，他的仁慈永遠常存。</td>
        <td width=100 class=r>100:5; 106:1<br>耶 33:11</td>
      </tr>
    </table>`;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result[1].verse.chapter).to.equal('107');
    expect(result[1].verse.verse).to.equal('1');
    expect(result[1].verse.text).to.equal('亞肋路亞！');
    expect(result[1].verse.prepend).to.equal(true);
  });

  it('extracts title, not verse', () => {
    const html = `<table>
      <tr valign=top>
        <td width=60>7:14</td>
        <td width=590>於是又叫<u>撒辣</u>的母親拿出書卷來，寫了婚書，並寫下他們怎麼按照<u>梅瑟</u>法律的規定，把她嫁給他為妻，並蓋了印。此後，他們便開始吃喝。</td>
        <td width=100 class=r></td>
      </tr>
    `;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result.length).to.equal(0);
  });

  describe('Esther', () => {
    it('extracts the extra text', () => {
      const html = `<table>
        <tr valign=top>
          <td width=500 colspan=2><b>[補錄乙]</b></td>
          <td width=100 class=r></td>
        </tr>
        <tr valign=top>
          <td width=750 colspan=3><br></td>
        </tr>
        <tr valign=top>
          <td width=500 colspan=2><b>諭文</b></td>
          <td width=100 class=r></td>
        </tr>
        <tr valign=top>
          <td width=60>1</td>
          <td width=590>諭文如下：「<u>薛西斯</u>大王欽命<u>印度</u>至<u>厄提約丕雅</u>之一百二十七省省長及其屬員事：</td>
          <td width=100 class=r></td>
        </tr>
      </table>`;

      const doc = getDocumentFromString(html);
      const table = doc.querySelector('table');
      const result = SectionExtractor.getSectionOfTable(table);
      expect(result[1].verse.isExtraChapter).to.equal(true);
      expect(result[1].verse.chapter).to.equal('乙');
    });
  });

  it('identifies is section or title (Songs)', () => {
    let text = '(寓意)1:1-4：新娘──選民在此表示';
    let result = SectionExtractor.isSection(text);
    expect(result).to.equal(false);

    result = SectionExtractor.isTitle(text);
    expect(result).to.equal(false);

    text = '第二場（2:17-3:5）描繪新娘（以民）重新所受的試探。';
    result = SectionExtractor.isSection(text);
    expect(result).to.equal(false);
  });

  it('does not extract cross reference verse (Psalms)', () => {
    const html = `<table>
      <tr valign=top>
        <td width=500 colspan=2><b>第五十五篇（54）</b></td>
        <td width=100 class=r>耶 9:1-8</td>
      </tr>
    </table>`;

    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SectionExtractor.getSectionOfTable(table);
    expect(result[0].title).to.equal('第五十五篇（54）');
  });
});
