const { expect } =require('chai');
const { getDocumentFromString } = require('utils');
const SpeakerExtractor = require('sbofmhk/speakerExtractor');

describe('Speaker extractor', () => {
  it('extracts speaker for saving', () => {
    const html = `<table>
      <tr valign=top>
        <td width=60></td>
        <td width=590><b>新娘</b></td>
        <td width=100 class=r></td>
      </tr>
      <tr valign=top>
        <td width=60>1:2</td>
        <td width=590>願君以熱吻與我接吻！因為你的愛撫甜於美酒。</td>
        <td width=100 class=r>4:10</td>
      </tr>
    </table>`;
    const doc = getDocumentFromString(html);
    const table = doc.querySelector('table');
    const result = SpeakerExtractor.getSpeakers(table);
    expect(result.length).to.equal(1);
    expect(result[0].speaker).to.equal('新娘');
    expect(result[0].chapter).to.equal('1');
    expect(result[0].verse).to.equal('2');
  });
});
