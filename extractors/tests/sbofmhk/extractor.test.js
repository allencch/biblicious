const sinon = require('sinon');
const { expect } = require('chai');
const { getDocumentFromString } = require('utils');
const {
  getExtraChapterFromRow,
  convertRowToVerse
} = require('sbofmhk/extractor');

describe('sbofmhk extractor', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => {
    sandbox.restore();
  });

  it('does not get extra chapter from row', () => {
    const html = `<table><tr valign=top>
    <td width=60>1:1</td>
    <td width=590>在起初天主創造了天地。</td>
    <td width=100 class=r>約 38-39<br>希 11:3<br>詠 8; 104<br>德 24:5<br>箴 8:22-31<br>若 1:1-3<br>哥 1:15-17</td>
  </tr></table>`;
    const doc = getDocumentFromString(html);
    const row = doc.querySelector('tr');
    const result = getExtraChapterFromRow(row);
    expect(result).to.equal(null);
  });

  it('gets extra chapter from row', () => {
    const html = `<table><tr valign=top>
    <td width=500 colspan=2><b>[補錄甲]</b></td>
    <td width=100 class=r></td>
  </tr></table>`;

    const doc = getDocumentFromString(html);
    const row = doc.querySelector('tr');
    const result = getExtraChapterFromRow(row);
    expect(result).to.equal('甲');
  });

  it('converts row to verse', () => {
    const html = `<table><tr valign=top>
    <td width=60>1:1</td>
    <td width=590>在起初天主創造了天地。</td>
    <td width=100 class=r>約 38-39<br>希 11:3<br>詠 8; 104<br>德 24:5<br>箴 8:22-31<br>若 1:1-3<br>哥 1:15-17</td>
  </tr></table>`;
    const doc = getDocumentFromString(html);
    const row = doc.querySelector('tr');
    const result = convertRowToVerse(row);
    expect(result.number).to.equal(1);
    expect(result.text).to.equal('在起初天主創造了天地。');
    expect(result.extraChapter).to.equal(null);
  });
});
