const { expect } = require('chai');
const DbService = require('dbService');
const Models = require('../../server/models');
const { Bible, Book, Chapter, Title, Verse, sequelize } = Models;

describe('dbService', () => {
  describe('Usual book', () => {
    let bible;
    let book;
    let chapter;
    let verse;
    beforeEach(async () => {
      bible = await Bible.create({ name: 'sigao' });
      book = await Book.create({ name: 'genesis' });
      chapter = await Chapter.create({ number: 1, bookId: book.id });
      verse = await Verse.create({
        text: 'in the beginning',
        number: 1,
        chapterId: chapter.id,
        bibleId: bible.id
      });
    });
    afterEach(async () => {
      await verse.destroy();
      await chapter.destroy();
      await book.destroy();
      await bible.destroy();

      await Title.truncate();
    });

    it('saves titles', async () => {
      const titles = [{
        title: 'Creation'
      }, {
        title: 'Beginning',
        verse: { chapter: '1', verse: '1' }
      }];

      await DbService.saveTitles(titles, 'sigao', 'genesis');
      await verse.reload();
      const title1 = await verse.getTitle();
      const title2 = await title1.getParent();

      expect(title1.title).to.equal('Beginning');
      expect(title2.title).to.equal('Creation');
    });

    it('saves title with partial verse', async () => {
      const titles = [{
        title: 'Neh',
        verse: { chapter: '1', verse: '1', text: ' something' } // add space at the beginning
      }];
      await DbService.saveTitles(titles, 'sigao', 'genesis');
      await verse.reload();
      const title = await verse.getTitle();

      expect(title.title).to.equal('Neh');
      expect(verse.text).to.equal('in the beginning something');
    });
  });

  describe('Esther', () => {
    let bible;
    let book;
    let chapter;
    let verse;
    beforeEach(async () => {
      bible = await Bible.create({ name: 'sigao' });
      book = await Book.create({ name: 'Esther' });
      chapter = await Chapter.create({ number: 3, bookId: book.id });
      verse = await Verse.create({
        text: '諭文如下',
        number: 1,
        chapterId: chapter.id,
        bibleId: bible.id,
        extraChapter: '乙'
      });
    });

    afterEach(async () => {
      await verse.destroy();
      await chapter.destroy();
      await book.destroy();
      await bible.destroy();

      await Title.truncate();
    });
    
    it('saves title of extra chapter', async () => {
      const titles = [{
        title: '[補錄乙]'
      }, {
        title: '諭文',
        verse: { isExtraChapter: true, chapter: '乙', verse: '1' }
      }];

      await DbService.saveTitles(titles, 'sigao', 'Esther');
      await verse.reload();
      let title = await verse.getTitle();
      expect(title.title).to.equal('諭文');
      title = await title.getTitle();
      expect(title.title).to.equal('[補錄乙]');
    });
  });
});
