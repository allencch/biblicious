const sinon = require('sinon');
const { expect } = require('chai');
const { getDocumentFromString } = require('utils');

describe('utils', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => {
    sandbox.restore();
  });

  it('gets document by HTML string', () => {
    const html = '<div>test<div class="test1">test2</div></div>';
    const doc = getDocumentFromString(html);
    const div = doc.querySelector('.test1');
    expect(div.innerHTML).to.equal('test2');
  });
});
