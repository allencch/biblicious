class Chapter {
  constructor() {
    this.verses = [];
  }

  addVerse(verse) {
    this.verses.push(verse);
  }

  getVerses() {
    return this.verses;
  }

  getVerse(index) {
    if (index >= this.verses.length) {
      throw 'Verse index is over';
    }
    return this.verses[index];
  }
}

module.exports = Chapter;
