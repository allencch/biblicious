const Books = require('constants/books.json');
const utils = require('utils');
const Verse = require('Verse');

const ROOT_URL = 'http://localhost:3100/usccb/www.usccb.org';
const BOOKS_OF_INTEREST = [
  {
    zhName: '創世紀',
  },
  // {
  //   zhName: '出谷紀',
  // },
  // {
  //   zhName: '肋未紀',
  // },
  // {
  //   zhName: '戶籍紀',
  // },
  // {
  //   zhName: '申命紀',
  // },
  // {
  //   zhName: '若蘇厄書',
  // },
  // {
  //   zhName: '民長紀',
  // },
  // {
  //   zhName: '盧德傳',
  // },
  // {
  //   zhName: '撒慕爾紀上',
  // },
  // {
  //   zhName: '撒慕爾紀下',
  // },
  // {
  //   zhName: '列王紀上',
  // },
  // {
  //   zhName: '列王紀下',
  // },
  // {
  //   zhName: '編年紀上',
  // },
  // {
  //   zhName: '編年紀下',
  // },
  // {
  //   zhName: '厄斯德拉上',
  // },
  // {
  //   zhName: '厄斯德拉下',
  // },
  // {
  //   zhName: '多俾亞傳',
  // },
  // {
  //   zhName: '友弟德傳',
  // },
  // {
  //   zhName: '艾斯德爾傳',
  // },
  // {
  //   zhName: '瑪加伯上',
  // },
  // {
  //   zhName: '瑪加伯下',
  // },
  // {
  //   zhName: '約伯傳',
  // },
  // {
  //   zhName: '聖詠集',
  // },
  // {
  //   zhName: '箴言',
  // },
  // {
  //   zhName: '訓道篇',
  // },
  // {
  //   zhName: '雅歌',
  // },
  // {
  //   zhName: '智慧篇',
  // },
  // {
  //   zhName: '德訓篇',
  // },
  // {
  //   zhName: '依撒意亞',
  // },
  // {
  //   zhName: '耶肋米亞',
  // },
  // {
  //   zhName: '哀歌',
  // },
  // {
  //   zhName: '巴路克',
  // },
  // {
  //   zhName: '厄則克耳',
  // },
  // {
  //   zhName: '達尼爾',
  // },
  // {
  //   zhName: '歐瑟亞',
  // },
  // {
  //   zhName: '岳厄爾',
  // },
  // {
  //   zhName: '亞毛斯',
  // },
  // {
  //   zhName: '亞北底亞',
  // },
  // {
  //   zhName: '約納',
  // },
  // {
  //   zhName: '米該亞',
  // },
  // {
  //   zhName: '納鴻',
  // },
  // {
  //   zhName: '哈巴谷',
  // },
  // {
  //   zhName: '索福尼亞',
  // },
  // {
  //   zhName: '哈蓋',
  // },
  // {
  //   zhName: '匝加利亞',
  // },
  // {
  //   zhName: '瑪拉基亞',
  // },
  // {
  //   zhName: '瑪竇福音',
  // },
  // {
  //   zhName: '馬爾谷福音',
  // },
  // {
  //   zhName: '路加福音',
  // },
  // {
  //   zhName: '若望福音',
  // },
  // {
  //   zhName: '宗徒大事錄',
  // },
  // {
  //   zhName: '羅馬書',
  // },
  // {
  //   zhName: '格林多前書',
  // },
  // {
  //   zhName: '格林多後書',
  // },
  // {
  //   zhName: '迦拉達書',
  // },
  // {
  //   zhName: '厄弗所書',
  // },
  // {
  //   zhName: '斐理伯書',
  // },
  // {
  //   zhName: '哥羅森書',
  // },
  // {
  //   zhName: '得撒洛尼前書',
  // },
  // {
  //   zhName: '得撒洛尼後書',
  // },
  // {
  //   zhName: '弟茂德前書',
  // },
  // {
  //   zhName: '弟茂德後書',
  // },
  // {
  //   zhName: '弟鐸書',
  // },
  // {
  //   zhName: '費肋孟書',
  // },
  // {
  //   zhName: '希伯來書',
  // },
  // {
  //   zhName: '雅各伯書',
  // },
  // {
  //   zhName: '伯多祿前書',
  // },
  // {
  //   zhName: '伯多祿後書',
  // },
  // {
  //   zhName: '若望一書',
  // },
  // {
  //   zhName: '若望二書',
  // },
  // {
  //   zhName: '若望三書',
  // },
  // {
  //   zhName: '猶達書',
  // },
  // {
  //   zhName: '若望默示錄',
  // },
];

function getBooks(booksOfInterest) {
  return booksOfInterest.map(b => {
    return Books.find(book => book.zhName === b.zhName);
  });
}

function cleanContent(content) {
  const fnrefs = content.querySelectorAll('.fnref, .enref, .fn, .en, table, .fncon');
  fnrefs.forEach(elem => {
    elem.parentNode.removeChild(elem);
  });
}

function extractVerses(content) {
  const verses = Array.from(content.querySelectorAll('a[name]'));
  const filtered = verses.filter(v => {
    return /\d+/.test(v.getAttribute('name'));
  });
  return filtered.map(v => {
    const bcv = v.querySelector('.bcv');
    if (!bcv) return null;

    // if (bcv.nextSibling)

    const num = Number(bcv.innerHTML);
    bcv.parentNode.removeChild(bcv);
    return {
      number: Number(num),
      text: v.textContent
    };
  }).filter(v => v);
}

async function extractChapter(book, chapterNum) {
  const name = book.name.toLowerCase();
  const url = `${ROOT_URL}/bible/${name}/${chapterNum}.html`;
  const doc = await utils.getDocumentFromPage(url);
  const content = doc.querySelector('#scribeI.contentarea');
  if (!content) {
    throw new Error('No content');
  }
  cleanContent(content);
  const verses = extractVerses(content);
  console.log(verses);
}

async function extractBook(book) {
  const { totalChapter } = book;
  for (let i = 0; i < 1; i++) {
    const chapterNum = i + 1;
    await extractChapter(book, chapterNum);
  }
}


async function main() {
  const books = getBooks(BOOKS_OF_INTEREST);
  for (let i = 0; i < books.length; i++) {
    const book = books[i];
    await extractBook(book);
  }
}

if (!module.parent) {
  main();
}

module.exports = {
  getBooks
};
