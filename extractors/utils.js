const JSDOM = require('jsdom').JSDOM;
const puppeteer = require('puppeteer');

const wrap = htmlContent => {
  const dom = new JSDOM(`<!DOCTYPE html><html><body>${htmlContent}</body></html>`);
  return dom.window.document;
};

const getDocumentFromPage = async url => {
  const browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-gpu',
      '--disable-dev-shm-usage'
    ]
  });
  const page = await browser.newPage();

  await page.goto(url);

  const result = await page.evaluate(() => {
    return document.querySelector('body').innerHTML;
  });

  await browser.close();
  return wrap(result);
};

const getDocumentFromString = html => {
  return wrap(html);
};

const brToNewline = elem => {
  elem.innerHTML = elem.innerHTML.replace(/<br\s*?\/?>/g, '[newline]');
  const text = elem.textContent;
  return text.replace(/\[newline\]/g, '\n');
};

module.exports = {
  getDocumentFromPage,
  getDocumentFromString,
  brToNewline
};
