const { Bible, Book, Chapter, SongSpeaker, Title, Verse, sequelize } = require('../server/models');

let dbStorage;
if (process.env.NODE_ENV === 'test') {
  dbStorage = '../server/db/database_test.sqlite3';
} else {
  dbStorage = '../server/db/database_development.sqlite3';
}
sequelize.options.storage = dbStorage;

async function saveVerseToDB(params) {
  const { bible, book, chapterNum, verse, successFn, order, transaction: t } = params;

  const bibleRecord = await Bible.findOne({ where: { name: bible }, transaction: t });
  const [bookRecord] = await Book.findOrCreate({ where: { name: book }, transaction: t });
  const [chapterRecord] = await Chapter.findOrCreate({
    where: {
      number: chapterNum,
      bookId: bookRecord.id
    },
    transaction: t
  });

  let result = await chapterRecord.update({ bookId: bookRecord.id }, { transaction: t });

  if (!result) {
    throw new Error('Failed to update chapter');
  }

  const [verseRecord] = await Verse.findOrCreate({
    where: {
      bibleId: bibleRecord.id,
      chapterId: chapterRecord.id,
      number: verse.number,
      extraChapter: verse.extraChapter || null
    },
    transaction: t
  });

  result = await verseRecord.update({
    text: verse.text,
    chapterId: chapterRecord.id,
    bible: bibleRecord.id,
    order,
    extraChapter: verse.extraChapter
  }, { transaction: t });

  if (!result) {
    throw new Error('Failed to update verse');
  }

  if (successFn) {
    successFn(verse, 'added');
  }
}

const saveChapterToDB = async ({
  bible,
  book,
  chapterNum,
  chapter,
  successFn
}) => {
  const verses = chapter.getVerses();
  await sequelize.transaction(async (t) => {
    for (let i = 0; i < verses.length; i++) {
      try {
        await saveVerseToDB({
          bible, book, chapterNum, verse: verses[i], order: i + 1, successFn,
          transaction: t
        });
      } catch(err) {
        console.log(err, book, chapterNum, verses[i].number);
      }
    }
  });
};

const prependToVerse = (verse, text, transaction) => {
  const pattern = new RegExp(`^${text}`);
  if (pattern.test(verse.text)) return null;

  return verse.update({ text: text + verse.text }, { transaction });
};

const appendToVerse = (verse, text, transaction) => {
  const pattern = new RegExp(`${text}\$`);
  if (pattern.test(verse.text)) return null;

  return verse.update({ text: verse.text + text }, { transaction });
};

const getVerseRecord = async (title, bibleRecord, bookRecord, transaction) => {
  if (title.verse.isExtraChapter) {
    return Verse.findOne({ where: {
      bibleId: bibleRecord.id,
      extraChapter: title.verse.chapter,
      number: title.verse.verse
    }, transaction });
  } else {
    const chapterRecord = await Chapter.findOne({ where: {
      bookId: bookRecord.id,
      number: title.verse.chapter
    }, transaction });

    return Verse.findOne({ where: {
      bibleId: bibleRecord.id,
      chapterId: chapterRecord.id,
      number: title.verse.verse
    }, transaction });
  }
};

const saveVerseTitle = async (title, bible, book, transaction) => {
  const bibleRecord = await Bible.findOne({ where: { name: bible }, transaction });
  const bookRecord = await Book.findOne({ where: { name: book }, transaction });

  const verseRecord = await getVerseRecord(title, bibleRecord, bookRecord, transaction);

  if (title.verse.text) {
    // Special case for Psalm 107
    if (title.verse.prepend) {
      await prependToVerse(verseRecord, title.verse.text, transaction);
    } else {
      // Special case for Nehemiah 7, append the partial verse
      await appendToVerse(verseRecord, title.verse.text, transaction);
    }
  }

  return Title.findOrCreate({
    where: {
      title: title.title,
      textId: verseRecord.id,
      textType: 'Verse'
    },
    transaction
  });
};

const saveTitleOfTitle = (title, prevTitle, transaction) => {
  return Title.findOrCreate({
    where: {
      title: title.title,
      textId: prevTitle.id,
      textType: 'Title'
    },
    transaction
  });
};

const saveTitles = (titles, bible, book) => {
  return sequelize.transaction(async (t) => {
    let prevTitle = null;
    for (let i = titles.length - 1; i >= 0; i--) {
      const title = titles[i];
      if (title.verse && title.verse.chapter && title.verse.verse) {
        ([prevTitle] = await saveVerseTitle(title, bible, book, t));
      } else {
        ([prevTitle] = await saveTitleOfTitle(title, prevTitle, t));
      }
    }
  });
};

const saveSongSpeakers = (speakers, bible, book) => {
  return sequelize.transaction(async t => {
    for (let i = 0; i < speakers.length; i++) {
      const speaker = speakers[i];

      const bibleRecord = await Bible.findOne({ where: { name: bible }, transaction: t });
      const bookRecord = await Book.findOne({ where: { name: book }, transaction: t });
      const chapterRecord = await Chapter.findOne({ where: {
        bookId: bookRecord.id,
        number: speaker.chapter
      }, transaction: t });

      const verseRecord = await Verse.findOne({ where: {
        bibleId: bibleRecord.id,
        chapterId: chapterRecord.id,
        number: speaker.verse
      }, transaction: t });

      await SongSpeaker.findOrCreate({
        where: {
          speaker: speaker.speaker,
          verseId: verseRecord.id
        },
        transaction: t
      });
    }
  });
};

module.exports = {
  saveChapterToDB,
  saveVerseToDB,
  saveTitles,
  saveSongSpeakers
};
