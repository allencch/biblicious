# Biblicious

## Requirement

* NodeJS (suggested 12.13.0 LTS), suggest to use `npm`.

## Initial setup

After `git clone` and `rvm` installed,

```
cd server
npm install
npx sequelize-cli db:migrate
```

### Initialize books (IMPORTANT!)

Run

```
cd server
node scripts/init_bible.rb
node scripts/init_bible_versions.rb
node scripts/add_chapter_numbers.rb
```

This will create all the books in the database. Can view the database file using `sqlitebrowser` or `sqliteman`.

## Start Server

```
cd server
npm start
```
Then use the web browser to open [http://localhost:3000](http://localhost:3000).


## Bibles

This repo doesn't provide bible. Get the bible from other channels.
Bibles can be downloaded using `httrack`.
Currently only supports bible from dsbiblecentre.

After download the bible, it should be put in the public folder as,

```
{project-root}/public/dsbiblecentre
{project-root}/public/dsbiblecentre/index.html
{project-root}/public/dsbiblecentre/sbbible.dsbiblecentre.org
{project-root}/public/dsbiblecentre/...
```

To access the page in Node server, http://localhost:3000/sbofmhk/127.0.0.1_3000/sbofmhk.html .

### Studium Biblicum websites differences

**dsbiblecentre** has some different traditional Chinese characters from **sbofmhk** .
For example, Psalm 94:9, dsbiblecentre renders as 聽不著, but sbofmk renders as 聽不着.
However, Hebrews 11:3, dsbiblecentre renders as 因着信德, but sbofmk renders as 因著信德.
But according the physical Studium Biblicum, word 着 instead of 著 is used in both cases.
**ccreadbible** also renders Psalm 94:9 as 聽不著 and same mistake as dsbiblecentre, which renders the same verse as 難道自己已聽不著 with extra character 已.
Therefore, **sbofmhk** looks like more reliable.


# Extractors

To extract the data, run the Node server, then

```
cd extractor
npm install # first time only
NODE_PATH=. node sbofmhk/extractor.js
```

## Studium Biblicum

### Development

To develop extractors, it requires `server` to run,

```
NODE_ENV=test npx sequelize-cli db:migrate # if there is db change
npm run start:test
```


## Useful commands

If you want to get the bibile yourself, you can download whole bible site using `httrack`.

Studium Biblicum

```
httrack -i +sbbible.dsbiblecentre.org/* 'http://127.0.0.1:3000/dsbiblecentre.html'
httrack -i +www.sbofmhk.org/pub/body/cpray/c1_online_bible/sbbible/* 'http://127.0.0.1:3000/sbofmhk.html'
```

NABRE

```
httrack -i +www.usccb.org/bible/* -mime:image/* -www.usccb.org/bible/readings/* -www.usccb.org/bible/scripture.cfm* -www.usccb.org/bible/*:* -www.usccb.org/bible/lecturas/* 'http://www.usccb.org/bible/books-of-the-bible/index.cfm'
```

# Webapp

```
cd webapp/
npm run server
npm start # call electron, optional
```


# Manual database fix

Mt 1:23 - Missing "」"


# Deployment

Using `fly.io`.
For `webapp`, we need to build with `NODE_ENV=production`, so that `.env.production` is used
