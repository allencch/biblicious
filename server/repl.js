const repl = require('repl').start({});
const models = require('./models');
const { sequelize } = models;

let dbStorage;
if (process.env.NODE_ENV === 'test') {
  dbStorage = '../server/db/database_test.sqlite3';
} else {
  dbStorage = '../server/db/database_development.sqlite3';
}
sequelize.options.storage = dbStorage;

repl.context.models = models;
