'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bible = sequelize.define('Bible', {
    name: DataTypes.STRING,
    abbrev: DataTypes.STRING,
    language: DataTypes.STRING
  }, {});
  Bible.associate = function(models) {
    // associations can be defined here
  };
  return Bible;
};