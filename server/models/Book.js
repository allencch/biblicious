'use strict';
module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
    order: DataTypes.INTEGER,
    name: DataTypes.STRING,
    abbrev: DataTypes.STRING,
    altName: DataTypes.STRING,
    nt: DataTypes.BOOLEAN,
    nameZh: DataTypes.STRING,
    abbrevZh: DataTypes.STRING,
    totalChapter: DataTypes.INTEGER
  }, {});
  Book.associate = function(models) {
    // associations can be defined here
  };
  return Book;
};