'use strict';
module.exports = (sequelize, DataTypes) => {
  const SongSpeaker = sequelize.define('SongSpeaker', {
    speaker: DataTypes.STRING,
    verseId: DataTypes.INTEGER
  }, {});
  SongSpeaker.associate = function(models) {
    SongSpeaker.belongsTo(models.Verse, { foreignKey: 'verseId', as: 'verse' });
    models.Verse.hasOne(SongSpeaker, { foreignKey: 'verseId', as: 'songSpeaker' });
  };
  return SongSpeaker;
};
