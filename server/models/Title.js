/**
 * The title before the target text, either a specific verse,
 * or another title.
 */
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Title = sequelize.define('Title', {
    title: DataTypes.STRING,
    textId: DataTypes.INTEGER, // This is child Id
    textType: DataTypes.ENUM('Verse', 'Title') // "Verse" or "Title"
  }, {});
  Title.associate = function(models) {
    Title.belongsTo(models.Title, { foreignKey: 'textId', as: 'child' });
    Title.hasOne(models.Title, {
      foreignKey: 'textId',
      as: 'parent',
      scope: {
        textType: 'Title'
      }
    });

    Title.belongsTo(models.Verse, { foreignKey: 'textId', as: 'verse' });
    models.Verse.hasOne(Title, {
      foreignKey: 'textId',
      as: 'title',
      scope: {
        textType: 'Verse'
      }
    });
  };

  // NOTE: Do not use arrow function, else cannot access "this"
  Title.prototype.getText = function() {
    if (this.textType === 'Verse') {
      return this.getVerse();
    }
    return this.getChild();
  };

  Title.prototype.getTitle = function() {
    return this.getParent();
  };

  return Title;
};
