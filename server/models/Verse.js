'use strict';
module.exports = (sequelize, DataTypes) => {
  const Verse = sequelize.define('Verse', {
    chapterId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Chapters'
      }
    },
    bibleId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Bibles'
      }
    },
    order: DataTypes.INTEGER,
    number: DataTypes.INTEGER,
    text: DataTypes.TEXT,
    extraChapter: DataTypes.STRING // For Esther's chapter
  }, {});
  Verse.associate = function(models) {
    Verse.belongsTo(models.Bible, { foreignKey: 'bibleId', as: 'bible' }),
    models.Bible.hasMany(Verse, { foreignKey: 'bibleId', as: 'verses' });

    Verse.belongsTo(models.Chapter, { foreignKey: 'chapterId', as: 'chapter' });
    models.Chapter.hasMany(Verse, { foreignKey: 'chapterId', as: 'verses' });
  };
  return Verse;
};
