'use strict';
module.exports = (sequelize, DataTypes) => {
  const Chapter = sequelize.define('Chapter', {
    bookId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Books'
      }
    },
    order: DataTypes.INTEGER,
    number: DataTypes.INTEGER,
    altName: DataTypes.STRING
  }, {});
  Chapter.associate = function(models) {
    Chapter.belongsTo(models.Book, { foreignKey: 'bookId', as: 'book' });
    models.Book.hasMany(Chapter, { foreignKey: 'bookId', as: 'chapters' });
  };
  return Chapter;
};
