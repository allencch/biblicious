const { Book } = require('../models');

const ot_books = {
  'Gen': 50,
  'Exod': 40,
  'Lev': 27,
  'Num': 36,
  'Deut': 34,
  'Josh': 24,
  'Judg': 21,
  'Ruth': 4,
  '1 Sam': 31,
  '2 Sam': 24,
  '1 Kgs': 22,
  '2 Kgs': 25,
  '1 Chr': 29,
  '2 Chr': 36,
  'Ezra': 10,
  'Neh': 13,
  'Tob': 14,
  'Jdt': 16,
  'Esth': 10,
  '1 Macc': 16,
  '2 Macc': 15,
  'Job': 42,
  'Ps': 150,
  'Prov': 31,
  'Eccl': 12,
  'Song': 8,
  'Wis': 19,
  'Sir': 51,
  'Isa': 66,
  'Jer': 52,
  'Lam': 5,
  'Bar': 6,
  'Ezek': 48,
  'Dan': 14,
  'Hos': 14,
  'Joel': 4,
  'Amos': 9,
  'Obad': 1,
  'Jonah': 4,
  'Mic': 7,
  'Nah': 3,
  'Hab': 3,
  'Zeph': 3,
  'Hag': 2,
  'Zech': 14,
  'Mal': 3
};

const nt_books = {
  'Matt': 28,
  'Mark': 16,
  'Luke': 24,
  'John': 21,
  'Acts': 28,
  'Rom': 16,
  '1 Cor': 16,
  '2 Cor': 13,
  'Gal': 6,
  'Eph': 6,
  'Phil': 4,
  'Col': 4,
  '1 Thess': 5,
  '2 Thess': 3,
  '1 Tim': 6,
  '2 Tim': 4,
  'Titus': 3,
  'Phlm': 1,
  'Heb': 13,
  'Jas': 5,
  '1 Pet': 5,
  '2 Pet': 3,
  '1 John': 5,
  '2 John': 1,
  '3 John': 1,
  'Jude': 1,
  'Rev': 22
};

async function main() {
  const books = {
    ...ot_books,
    ...nt_books
  };
  await Object.keys(books).map(async (key) => {
    const number = books[key];
    return await Book.update({ totalChapter: number }, { where: { abbrev: key } });
  });
  console.log('Done');
}

main();
