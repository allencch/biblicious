const { Op } = require('sequelize');
const models = require('../models');
const { Verse, Bible } = models;

async function main() {
  const verses = await Verse.findAll({
    include: [{
      model: Bible, as: 'bible'
    }],
    where: {
      '$bible.abbrev$': '思高',
      text: { [Op.like]: '%zd2%' }
    }
  });

  for (let i = 0; i < verses.length; i++) {
    const verse = verses[i];
    const text = verse.text.replace(/zd2/g, '「');
    console.log(text);
    await verse.update({
      text
    });
  }

  console.log('Curly brace replaced');
}

main();
