const models = require('../models');
const { Bible } = models;

async function main() {
  await Bible.truncate();
  await Bible.create({
    name: 'Studium Biblicum',
    abbrev: '思高',
    language: 'zh'
  });
}

main();
