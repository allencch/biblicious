const { Op } = require('sequelize');
const { Title, Verse } = require('../models');

async function getTitle(text, level) {
  const title = await text.getTitle();
  if (!title) {
    return level;
  }
  return await getTitle(title, level + 1);
}

async function main() {
  let level = 0;
  const verses = await Verse.findAll({
    include: {
      model: Title,
      required: true
    }
  });

  for (let i = 0; i < verses.length; i++) {
    const verse = verses[i];
    const titleLevel = await getTitle(verse, 0);
    if (titleLevel > level) {
      level = titleLevel;
    }
  }

  console.log(`Max title level: ${level}`);
}

main();
