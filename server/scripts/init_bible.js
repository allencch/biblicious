const models = require('../models');
const { Book } = models;

const ot_books = [
  'Genesis', 'Exodus', 'Leviticus', 'Numbers', 'Deuteronomy',
  'Joshua', 'Judges', 'Ruth',
  '1 Samuel', '2 Samuel', '1 Kings', '2 Kings', '1 Chronicles', '2 Chronicles',
  'Ezra', 'Nehemiah',
  'Tobit', 'Judith', 'Esther',
  '1 Maccabees', '2 Maccabees',
  'Job', 'Psalms', 'Proverbs', 'Ecclesiastes', 'Song of Songs', 'Wisdom', 'Sirach',
  'Isaiah', 'Jeremiah', 'Lamentations', 'Baruch', 'Ezekiel', 'Daniel',
  'Hosea', 'Joel', 'Amos', 'Obadiah', 'Jonah', 'Micah',
  'Nahum', 'Habakkuk', 'Zephaniah', 'Haggai', 'Zechariah', 'Malachi'
];
const ot_books_abbrev = [
  'Gen', 'Exod', 'Lev', 'Num', 'Deut',
  'Josh', 'Judg', 'Ruth',
  '1 Sam', '2 Sam', '1 Kgs', '2 Kgs', '1 Chr', '2 Chr',
  'Ezra', 'Neh',
  'Tob', 'Jdt', 'Esth',
  '1 Macc', '2 Macc',
  'Job', 'Ps', 'Prov', 'Eccl', 'Song', 'Wis', 'Sir',
  'Isa', 'Jer', 'Lam', 'Bar', 'Ezek', 'Dan',
  'Hos', 'Joel', 'Amos', 'Obad', 'Jonah', 'Mic',
  'Nah', 'Hab', 'Zeph', 'Hag', 'Zech', 'Mal'
];
const ot_books_zh = [
  '創世紀', '出谷紀', '肋未紀', '戶籍紀', '申命紀',
  '若蘇厄書', '民長紀', '盧德傳',
  '撒慕爾紀上', '撒慕爾記下', '列王紀上', '列王紀下', '編年紀上', '編年記下',
  '厄斯德拉上', '厄斯德拉下',
  '多俾亞傳', '友弟德傳', '艾斯德爾傳',
  '瑪加伯上', '瑪加伯下',
  '約伯傳', '聖詠集', '箴言', '訓道篇', '雅歌', '智慧篇', '德訓篇',
  '依撒意亞', '耶肋米亞', '哀歌', '巴路克', '厄則克爾', '達尼爾',
  '甌瑟亞', '岳厄爾', '亞毛斯', '亞北底亞', '約納', '米該亞',
  '納鴻', '哈巴谷', '索福尼亞', '哈蓋', '匝加利亞', '瑪拉基亞'
];
const ot_books_abbrev_zh = [
  '創', '出', '肋', '戶', '申',
  '蘇', '民', '盧',
  '撒上', '撒下', '列上', '列下', '編上', '編下',
  '厄上', '厄下',
  '多', '友', '艾',
  '加上', '加下',
  '約', '詠', '箴', '訓', '歌', '智', '德',
  '依', '耶', '哀', '巴', '則', '達',
  '甌', '岳', '亞', '北', '納', '米',
  '鴻', '哈', '索', '蓋', '匝', '拉'
];

const nt_books = [
  'Matthew', 'Mark', 'Luke', 'John', 'Acts',
  'Romans', '1 Corinthians', '2 Corinthians', 'Galatians', 'Ephesians',
  'Philippians', 'Colossians', '1 Thessalonians', '2 Thessalonians',
  '1 Timothy', '2 Timothy', 'Titus', 'Philemon', 'Hebrews',
  'James', '1 Peter', '2 Peter', '1 John', '2 John', '3 John', 'Jude',
  'Revelation'
];

const nt_books_abbrev = [
  'Matt', 'Mark', 'Luke', 'John', 'Acts',
  'Rom', '1 Cor', '2 Cor', 'Gal', 'Eph',
  'Phil', 'Col', '1 Thess', '2 Thess',
  '1 Tim', '2 Tim', 'Titus', 'Phlm', 'Heb',
  'Jas', '1 Pet', '2 Pet', '1 John', '2 John', '3 John', 'Jude',
  'Rev'
];

const nt_books_zh = [
  '瑪竇福音', '馬爾谷福音', '路加福音', '若望福音', '宗徒大事錄',
  '羅馬書', '格林多前書', '格林多後書', '迦拉達書', '厄弗所書',
  '斐理伯書', '哥羅森書', '得撒洛尼前書', '得撒洛尼後書',
  '弟茂德前書', '弟茂德後書', '弟鐸書', '費肋孟書', '希伯來書',
  '雅各伯書', '伯多祿前書', '伯多祿後書', '若望一書', '若望二書', '若望三書', '猶達書',
  '若望默示錄'
];

const nt_books_abbrev_zh = [
  '瑪', '谷', '路', '若', '宗',
  '羅', '格前', '格後', '迦', '弗',
  '斐', '哥', '得前', '得後',
  '弟前', '弟後', '鐸', '費', '希',
  '雅', '伯前', '伯後', '若一', '若二', '若三', '猶',
  '默'
];

async function main() {
  await Book.truncate();
  await ot_books.map(async (book, index) => {
    return await Book.create({
      order: index + 1,
      name: book,
      abbrev: ot_books_abbrev[index],
      nameZh: ot_books_zh[index],
      abbrevZh: ot_books_abbrev_zh[index],
      nt: false
    });
  });

  const ntIndexStart = ot_books.length + 1;
  await nt_books.map(async (book, index) => {
    return await Book.create({
      order: index + ntIndexStart,
      name: book,
      abbrev: nt_books_abbrev[index],
      nameZh: nt_books_zh[index],
      abbrevZh: nt_books_abbrev_zh[index],
      nt: true
    });
  });

  await Book.update({ altName: 'Qoheleth' }, { where: { abbrev: 'Eccl' } });
  await Book.update({ altName: 'Canticle of Canticles' }, { where: { abbrev: 'Song' } });
  await Book.update({ altName: 'Ecclesisticus' }, { where: { abbrev: 'Sir' } });
  await Book.update({ altName: 'Apocalypse' }, { where: { abbrev: 'Rev' } });

  console.log('Done');
}

main();
