const { Op } = require('sequelize');
const models = require('../models');
const { Verse, Bible } = models;

async function main() {
  const verses = await Verse.findAll({
    include: [{
      model: Bible, as: 'bible'
    }],
    where: {
      '$bible.abbrev$': '思高',
      text: { [Op.like]: '%  %' }
    }
  });

  for (let i = 0; i < verses.length; i++) {
    const verse = verses[i];
    if (/（休止）/.test(verse.text)) continue;

    const text = verse.text.replace(/\s+/g, '');
    console.log(text);
    await verse.update({
      text
    });
  }

  console.log('Spaces removed');
}

main();
