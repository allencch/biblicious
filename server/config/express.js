const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const { routesSetup } = require('../routes');

const app = express();

// This for the static downloaded webpages by httrack
app.use(express.static('../public'));
app.use(cors());
app.use(morgan('tiny'));

routesSetup(app);

module.exports = {
  app
};
