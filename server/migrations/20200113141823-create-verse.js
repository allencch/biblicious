'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Verses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      chapterId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Chapters'
          },
          key: 'id'
        }
      },
      bibleId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'Bibles'
          },
          key: 'id'
        }
      },
      order: {
        type: Sequelize.INTEGER
      },
      number: {
        type: Sequelize.INTEGER
      },
      text: {
        type: Sequelize.TEXT
      },
      extraChapter: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Verses');
  }
};
