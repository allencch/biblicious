'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Books', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      order: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      abbrev: {
        type: Sequelize.STRING
      },
      altName: {
        type: Sequelize.STRING
      },
      nt: {
        type: Sequelize.BOOLEAN
      },
      nameZh: {
        type: Sequelize.STRING
      },
      abbrevZh: {
        type: Sequelize.STRING
      },
      totalChapter: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Books');
  }
};