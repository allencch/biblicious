'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Titles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      textId: {
        type: Sequelize.INTEGER
      },
      textType: {
        type: Sequelize.ENUM('Verse', 'Title')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    return queryInterface.addIndex('Titles', ['textId']);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Titles');
  }
};
