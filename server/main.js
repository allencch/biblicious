/* NOTE: Deprecated */
const path = require('path');
const express = require('express');

// Express
const app = express();
const port = 5000;

app.use('/', express.static(path.join('ui')));
app.get('/', (req, res) => {
  res.sendFile(path.join('ui', 'index.html'));
});

app.listen(port, () => {
  console.log(`Listening on port ${port}.`);
});
