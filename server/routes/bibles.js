const { Router } = require('express');
const { Bible } = require('../models');

function biblesRoute(app) {
  const route = Router();

  route.get('/', async (req, res) => {
    const bibles = await Bible.findAll();
    res.json(bibles);
  });

  app.use('/bibles', route);
}

module.exports = {
  biblesRoute
};
