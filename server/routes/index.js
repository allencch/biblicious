const { biblesRoute } = require('./bibles');
const { booksRoute } = require('./books');
const { searchRoute } = require('./search');

function routesSetup(app) {
  biblesRoute(app);
  booksRoute(app);
  searchRoute(app);
}

module.exports = {
  routesSetup
};
