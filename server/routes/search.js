const { Op } = require('sequelize');
const { Router } = require('express');
const { query, validationResult } = require('express-validator');
const { Verse, Chapter, Book, Bible } = require('../models');

const PAGE_SIZE = 50;

function getVerses(query) {
  const { search, page, size } = query;
  const offset = ((page || 1) - 1) * (size || PAGE_SIZE);

  const params = {
    include: [{
      model: Chapter, as: 'chapter',
      include: {
        model: Book, as: 'book'
      }
    }, {
      model: Bible, as: 'bible'
    }],
    limit: size || PAGE_SIZE,
    offset,
    where: {
      text: { [Op.like]: `%${decodeURI(search)}%` }
    }
  };
  return Verse.findAndCountAll(params);
}

function searchRoute(app) {
  const route = Router();

  route.get('/', [
    query('search').isString().optional(),
    query('page').isNumeric().optional().toInt(),
    query('size').isNumeric().optional().toInt()
  ], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const { count, rows: verses } = await getVerses(req.query);
    const totalPages = Math.ceil(count / (req.size || PAGE_SIZE));

    return res.send({ data: verses, totalPages, totalItems: count });
  });

  app.use('/search', route);
}

module.exports = {
  searchRoute
};
