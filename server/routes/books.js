const { Router } = require('express');
const { Bible, Book, Chapter, SongSpeaker, Title, Verse } = require('../models');


function flattenVerses(chapter) {
  const verses = [];
  chapter.verses.forEach(verse => {
    if (verse.title) {
      if (verse.title.parent) {
        if (verse.title.parent.parent) {
          verses.push({ ...verse.title.parent.parent.get(), level: 3 }); // Level 3
        }
        verses.push({ ...verse.title.parent.get(), level: 2 }); // Level 2
      }
      verses.push({ ...verse.title.get(), level: 1 }); // Level 1
    }
    verses.push({ ...verse.get() });
  });
  return verses;
}

function flattenBookTitles(book) {
  const flattened = { ...book.get() };
  flattened.chapters = book.chapters.map(chapter => {
    const verses = flattenVerses(chapter);
    return({
      ...chapter.get(),
      verses
    });
  });
  return flattened;
}

function booksRoute(app) {
  const route = Router();

  route.get('/', async (req, res) => {
    const books = await Book.findAll();
    res.json(books);
  });

  route.get('/:bible/:abbrev', async (req, res) => {
    const { bible, abbrev } = req.params;
    try {
      const book = await Book.findOne({
        include: {
          model: Chapter,
          as: 'chapters',
          include: {
            model: Verse,
            as: 'verses',
            include: [{
              model: Bible,
              as: 'bible',
              where: { abbrev: bible }
            }, {
              model: Title, // Level 1
              as: 'title',
              include: {
                model: Title, // Level 2
                as: 'parent',
                include: {
                  model: Title, // Level 3
                  as: 'parent'
                }
              }
            }, {
              model: SongSpeaker,
              as: 'songSpeaker',
              required: false
            }]
          },
        },
        where: { abbrev },
        order: [
          ['order', 'ASC'],
          [
            { model: Chapter, as: 'chapters' },
            'number',
            'ASC'
          ],
          [
            { model: Chapter, as: 'chapters' },
            { model: Verse, as: 'verses' },
            'order',
            'ASC'
          ]
        ]
      });
      res.json(flattenBookTitles(book));
    } catch(e) {
      console.log(e);
    }
  });

  app.use('/books', route);
}

module.exports = {
  booksRoute
};
