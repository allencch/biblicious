/* global process */
const Sequelize = require('sequelize');
const dotenv = require('dotenv');
dotenv.config();

const sequelize = new Sequelize('main', '', '', {
  dialect: 'sqlite',
  storage: process.env.DATABASE
});

sequelize
  .authenticate()
  .then(() => {
    console.log('DB Connection success.');
  })
  .catch(err => {
    console.error('Unable to connect to database: ', err);
  });

module.exports = sequelize;
