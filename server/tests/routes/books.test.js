const request = require('supertest');
const { expect } = require('chai');
const { app } = require('config/express');

describe('Books router', () => {
  it('gets books', async () => {
    const url = '/books/思高/Jude';
    const res = await request(app).get(encodeURI(url));
    expect(res.status).to.equal(200);

    const [chapter] = res.body.chapters;
    expect(chapter.verses.length).to.equal(32);
  });
});
