const request = require('supertest');
const { expect } = require('chai');
const { app } = require('config/express');

describe('Books router', () => {
  it('gets books', async () => {
    const query = new URLSearchParams({
      search: '一'
    });
    const url = `/search?${query.toString()}`;
    const res = await request(app).get(url);
    expect(res.status).to.equal(200);
    expect(res.body.totalPages).to.equal(182);
    expect(res.body.data.length).to.equal(50);
  });
});
