const { expect } = require('chai');
const models = require('models');
const { Verse, Book } = models;

describe('Verse', () => {
  xit('gets book', async () => {
    const verse = await Verse.findOne();
    const chapter = await verse.getChapter();
    const book = await chapter.getBook();
    expect(book.abbrev).to.equal('Gen');
  });

  xit('gets book through association', async () => {
    const verse = await Verse.findOne({
      include: [{
        model: Book,
        as: 'books'
      }]
    });
    const { books } = verse;
    expect(books[0].abbrev).to.equal('Gen');
  });
});
