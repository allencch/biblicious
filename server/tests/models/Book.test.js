const { expect } = require('chai');
const { Verse, Book, Bible, Chapter } = require('models');

describe('Book', () => {
  it('gets verses', async () => {
    const book = await Book.findOne({ where: { abbrev: 'Gen' } });
    const chapters = await book.getChapters({ where: { number: 1 } });
    const [chapter] = chapters;
    const verses = await chapter.getVerses();
    expect(verses.length).to.equal(31);
  });

  it('gets verses of a specific book', async () => {
    const bible = '思高';
    const book = await Book.findOne({
      include: {
        model: Chapter,
        as: 'chapters',
        include: [{
          model: Verse,
          as: 'verses',
          include: {
            model: Bible,
            as: 'bible',
            where: { abbrev: bible }
          }
        }],
      },
      where: { abbrev: 'Gen' }
    });
    const [chapter] = book.chapters;
    expect(book.chapters.length).to.equal(50);
    expect(chapter.verses.length).to.equal(31);
  });
});
