const { expect } = require('chai');
const models = require('models');
const { Title, Verse } = models;

// Run this in "test" environment
xdescribe('Title model', () => {
  it('belongs to a verse', async () => {
    const verse = await Verse.create({ text: 'testing' });
    const title = await Title.create({ title: 'title', textType: 'Verse', textId: verse.id });
    const result = await title.getText();
    expect(result.text).to.equal('testing');

    await title.destroy();
    await verse.destroy();
  });

  it('belongs to another title', async () => {
    const title1 = await Title.create({ title: 'title1' });
    const title2 = await Title.create({ title: 'title2', textType: 'Title', textId: title1.id });
    const result = await title2.getText();
    expect(result.title).to.equal('title1');

    await title2.destroy();
    await title1.destroy();
  });
});
