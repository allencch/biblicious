const { app } = require('config/express');

const server = app.listen(process.env.PORT || 3100, () => {
  const port = server.address().port;
  console.log(`Server started at http://localhost:${port}`);
});
