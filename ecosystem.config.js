module.exports = {
  apps : [{
    name: 'Server',
    cwd: 'server',
    script: 'index.js',
    watch: '.',
    ignore_watch: ['db'],
    env: {
      NODE_PATH: '.'
    }
  }, {
    name: 'Webapp',
    cwd: 'webapp',
    script: './node_modules/@vue/cli-service/bin/vue-cli-service.js',
    args: 'serve',
    env: {
      NODE_PATH: '.'
    }
  }]
};
