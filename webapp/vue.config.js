module.exports = {
  publicPath: '',
  chainWebpack: config => {
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap(options => {
        options.compilerOptions.whitespace = 'preserve';
        return options;
      });
  }
}
