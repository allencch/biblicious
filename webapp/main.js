const { app, BrowserWindow } = require('electron');
const { menuAppendFind } = require('./electronHelpers/menu');
const { setupIpcMain, setupIpcRenderer } = require('./electronHelpers/ipc');

let win;

const createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: { nodeIntegration: true }
  });
  win.loadFile('dist/index.html');

  // Enable this for inspector
  // win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });

  return win;
};

app.whenReady().then(() => {
  const window = createWindow();
  menuAppendFind(window);
  setupIpcMain(window);

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });
  app.on('active', () => {
    if (win === null) {
      createWindow();
    }
  });
});
