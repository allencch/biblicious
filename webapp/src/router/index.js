import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/Home';
import Books from '@/pages/Books';
import Book from '@/pages/Book';
import Search from '@/pages/Search';

Vue.use(VueRouter)

const routes = [
  {
    path: '/', component: Home,
    children: [
      { path: 'bible/:bible/books', component: Books },
      { path: 'bible/:bible/books/:abbrev', component: Book },
      { path: 'search', component: Search }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
