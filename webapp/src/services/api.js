/* global process */
const endpoint = process.env.VUE_APP_ENDPOINT;

export const getBibles = () => {
  return fetch(`${endpoint}/bibles`)
    .then(res => res.json());
};

export const getBooks = () => {
  return fetch(`${endpoint}/books`)
    .then(res => res.json());
};

export const getBook = (bible, abbrev) => {
  return fetch(`${endpoint}/books/${bible}/${abbrev}`)
    .then(res => res.json());
};

export const searchVerses = (params) => {
  const query = new URLSearchParams(params);
  return fetch(`${endpoint}/search?${query.toString()}`)
    .then(res => res.json());
};
