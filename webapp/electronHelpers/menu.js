const { Menu, MenuItem } = require('electron');
const { openFindModal } = require('./findModal');
const { ipcFindNext } = require('./ipc');

const menuAppendFind = (window) => {
  const menu = Menu.getApplicationMenu();
  menu.append(new MenuItem({
    label: '&Find',
    submenu:[
      new MenuItem({
        label: 'Find...',
        accelerator: 'CmdOrCtrl+F',
        click: () => {
          openFindModal();
        }
      }),
      new MenuItem({
        label: 'Find Next',
        accelerator: 'F3',
        click: () => {
          ipcFindNext(window);
        }
      })
    ]
  }));
  Menu.setApplicationMenu(menu);
};

module.exports = {
  menuAppendFind
};
