const { ipcMain } = require('electron');
const { openFindModal } = require('./findModal');

const ipcContext = {
  findText: ''
};

const setupIpcMain = (window) => {
  ipcMain.on('my-find-text', (event, arg) => {
    console.log('ipc-main', arg);
    ipcContext.findText = arg;
    event.reply('close-modal', true);

    window.webContents.findInPage(ipcContext.findText);
  });
};

const ipcFindNext = window => {
  if (!ipcContext.findText) {
    openFindModal();
    return;
  }

  window.webContents.findInPage(ipcContext.findText);
};

module.exports = {
  ipcContext,
  setupIpcMain,
  ipcFindNext
};
