const path = require('path');
const { BrowserWindow } = require('electron');

let window;
const openFindModal = (parent) => {
  window = new BrowserWindow({
    width: 240,
    height: 50,
    parent,
    show: true,
    modal: true,
    alwaysOnTop: true,
    title: 'Find',
    autoHideMenuBar: true,
    webPreferences: { nodeIntegration: true }
  });

  window.on('closed', () => {
    window = null;
  });
  window.loadFile(path.join(__dirname, 'findModal.html'));
};

module.exports = {
  openFindModal
};
