import './assets/main.css'

import { createApp } from 'vue'
import BalmUI from 'balm-ui'; // Official Google Material Components
import BalmUIPlus from 'balm-ui-plus'; // BalmJS Team Material Components
import App from './App.vue'
import router from './router'
import 'balm-ui-css';

const app = createApp(App)

app.use(router)
app.use(BalmUI)
app.use(BalmUIPlus)

app.mount('#app')
