import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Books from '@/pages/Books.vue'
import Book from '@/pages/Book.vue';
import Search from '@/pages/Search.vue';

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      children: [
        { path: 'bible/:bible/books', component: Books },
        { path: 'bible/:bible/books/:abbrev', component: Book },
        { path: 'search', component: Search }
      ]
    }
  ]
})

export default router
